# Context

A client sends their invoice(s). After invoice verification, invoice is purchased and invoice amount is paid to the client. A small portion of the invoice amount is held back as a fee. The fee depends on the amount of days that an invoice is outstanding. Therefore it is important to know whether and when a debtor is going to pay.



# Data set

The data set you have been given is a data set that is made public by IBM. It contains information about invoices that have been factored. It contains 2466 rows and 10 columns. The columns are as follows

| Column         | Type    | Description                                                    |
| Country code   | Numeric | Code of the country of the client                              |
| Customer id    | Text    | Customer id                                                    |
| Paperless date | Date    | Date when client switched from paper to electronic             |
| Invoice number | Numeric | Invoice number                                                 |
| Invoice date   | Date    | Date the invoice was issued                                    |
| Due date       | Date    | Date the invoice was due                                       |
| Invoice amount | Money   | Amount of the invoice                                          |
| Disputed       | Boolean | Whether there was a conflict on the goods or service delivered |
| Paperless bill | Enum    | Method with which the invoice was sent                         |
| Days late      | Numeric | The amount of days the invoice was paid late                   |



# Goal

Create a model that predicts whether an invoice is paid late (yes or no).

Use the last column (Days late) as your output, where 0 means no and any value higher than 0 means yes.

In order to create your solution please take the following steps:

• Clean the data
• Feature selection
• Build a classification pipeline
• Train two classifiers (using different algorithms)